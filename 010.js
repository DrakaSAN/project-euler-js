function isPrime(n) {
	let root = Math.ceil(Math.sqrt(n)),
		i = 2;

	if(n === 2) {
		return true;
	}

	while(i < root) {
		if(n % i == 0) {
			return false;
		}
		i = i + 1;
	}


	if(n % root === 0) {
		return false;
	}

	return true;
}

let limit = 2000000,
	primes = [],
	i = 0;

while(i < limit) {
	if(isPrime(i)) {
		primes.push(i);
	}
	i = i + 1;
}
console.log(primes);
console.log(primes.reduce((acc, val) => {return acc + val;}));