'use strict';

let i = 2,
	current = 1,
	max = 0,
	ndivisor = 0;

function getDivisor(n) {
	let divisor = [],
		i = 1;

	while(i <= n) {
		if(n % i === 0) {
			divisor.push(i);
		}
		i = i + 1;
	}

	return divisor;
}

ndivisor = getDivisor(current).length;
// console.log(current + ': ' + getDivisor(current));
while(ndivisor < 500) {
	current = current + i;
	ndivisor = getDivisor(current).length;
	max = Math.max(ndivisor, max);
	console.log(i + ' / ' + current + ': ' + ndivisor + ' max: ' + max);
	i = i + 1;
}
// console.log(current + ': ' + getDivisor(current).length);
