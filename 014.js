'use strict';

function collatz(n) {
	let step = 1;

	// process.stdout.write('' + n);

	while(n !== 1) {
		if(n % 2 === 0) {
			n = n / 2;
		} else {
			n = 3 * n + 1;
		}

		// process.stdout.write(' => ' + n);

		step = step + 1;
	}

	// process.stdout.write('\nSteps: ' + step + '\n');
	return step;
}

let i = 1,
	limit = 1000000,
	max = 0,
	maxI = 0,
	c = 0;

while(i < limit) {
	c = collatz(i);

	if(c > max) {
		max = c;
		maxI = i;
		console.log('Max: ' + maxI + ': ' + max);
	}

	i = i + 1;
}

// collatz(13);