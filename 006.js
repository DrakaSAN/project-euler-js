console.log(getSquareSum(100) - getSumSquare(100));

function getSumSquare(n) {
	let sum = 0,
		i = 1;

	while(i < n + 1) {
		sum = sum + (i * i);
		i = i + 1;
	}

	return sum;
}

function getSquareSum(n) {
	let sum = 0,
		i = 1;

	while(i < n + 1) {
		sum = sum + i;
		i = i + 1;
	}

	return sum * sum;
}
