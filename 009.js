'use strict';

// function checkPythagorian(a, b, c) {
// 	if(!(a < b) || !(b < c)) {
// 		return false;
// 	}
// 	if(((a*a) + (b*b)) !== (c*c)) {
// 		// console.log('false: ' + a + ' + ' + b + ' = ' + Math.sqrt(a*a + b*b) +  ' != ' + c);
// 		// console.log('       ' + (a * a) + ' + ' + (b * b) + ' = ' + (a*a + b*b) + ' != ' + (c * c));
// 		return false;
// 	}
// 	return true;
// }

function getPythagorianSum(a, b) {
	let c = a * a + b * b;
	return a + b + Math.sqrt(c);
}

let a = 1, b = 2;

while(b < 1000) {
	while(a < b) {
		if(getPythagorianSum(a, b) === 1000) {
			console.log(a * b * Math.sqrt(a * a + b * b));
		}
		a = a + 1;
	}
	a = 1;
	b = b + 1;
}

// console.log(getPythagorianSum(3, 4, 5));
