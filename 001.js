'use strict';

function getSumOfMultiplesOf(max, multiples) {
	let i = 0,
		sum = 0,
		isMultiple = false;

	while(i < max) {
		multiples.map((multiple) => {
			if(i % multiple === 0) {
				isMultiple = true;
			}
		});
		
		if(isMultiple) {
			sum = sum + i;
			isMultiple = false;
		}
		
		i = i + 1;
	}


	return sum;
}

console.log(getSumOfMultiplesOf(1000, [3, 5]));