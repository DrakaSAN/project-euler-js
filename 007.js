let prime_numbers = 1,
	i = 2;

while(prime_numbers < 10002) {
	if(isPrime(i)) {
		console.log(prime_numbers + ': ' + i);
		prime_numbers = prime_numbers + 1;
	}
	i = i + 1;
}

function isPrime(n) {
	let root = Math.ceil(Math.sqrt(n)),
		i = 2;

	if(n === 2) {
		return true;
	}

	while(i < root) {
		if(n % i == 0) {
			return false;
		}
		i = i + 1;
	}


	if(n % root === 0) {
		return false;
	}

	return true;
}
