'use strict';

function double(s) {
    let i = 0,
        overflow = false;

    // console.log('double(' + s + ')');

    s = s.split('').reverse().map((n) => {return parseInt(n);});

    while(i < s.length) {
        s[i] = s[i] * 2;
        if(overflow) {
            s[i] = s[i] + 1;
            overflow = false;
        }
        if(s[i] > 9) {
            s[i] = s[i] - 10;
            overflow = true;
        }
        i = i + 1;
    }

    if(overflow) {
        s.push('1');
    }

    s = s.reverse().map((n) => {return '' + n;}).join('');

    // console.log('=> ' + s);

    return s;
}

function pow2(power) {
    let i = 0,
        val = '1';
    
    while(i < power) {
        val = double(val);
        i = i + 1;
    }

    return val;
}

let input = pow2(process.argv[2] || 1),
    output = input.split('').map((n) => {return parseInt(n);}).reduce((acc, val) => {
        // console.log(acc + ' + ' + val);
        return acc + val;
    }, 0);

console.log(input + ' => ' + output);
