function fibonacciSumOfEven(a, b, max, sum) {
	if(a > max) {
		return sum;
	}
	if(a % 2 === 0) {
		// console.log('+' + a + ' => ' + (sum + a));
		return fibonacciSumOfEven(b, b + a, max, sum + a);
	} else {
		return fibonacciSumOfEven(b, b + a, max, sum);
	}
}

console.log(fibonacciSumOfEven(1, 2, 4000000, 0));
