'use strict';

function getMaxPrimeFactorOf(n) {
	let root = Math.sqrt(n),
		maxFactor = 0,
		i = 0;

	while(i < Math.trunc(root)) {
		if(isPrime(i)) {
			if(n % i === 0) {
				maxFactor = i;
			}
		}
		i = i + 1;
	}

	return maxFactor;
}

function isPrime(n) {
	let root = Math.ceil(Math.sqrt(n)),
		i = 2;

	if(n === 2) {
		return true;
	}

	while(i < root) {
		if(n % i == 0) {
			return false;
		}
		i = i + 1;
	}


	if(n % root === 0) {
		return false;
	}

	return true;
}

console.log(getMaxPrimeFactorOf(600851475143));

// let i = 0;
// while(i < 15) {
// 	console.log(i + ' => ' + isPrime(i));
// 	i = i + 1;
// }

// console.log(isPrime(8));