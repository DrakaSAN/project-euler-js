function getLargestPalindrome(digit) {
	let largest = 0,
		max = 1;

	let i = 0;
	while(i < digit) {
		max = max * 10;
		i = i + 1;
	}

	let min = max / 10;
	max = max - 1;
	i = max;
	let j = max;

	while(i > min) {
		j = max;
		while(j > min) {
			if(isPalindrome(i * j)) {
				largest = Math.max(largest, i * j);
				if(largest === i * j) {
					console.log('=> ' + largest);
				}
			}
			j = j - 1;
		}
		i = i - 1;
	}

	return largest;
}

function isPalindrome(n) {
	let n_s = '' + n,
		limit = n_s.length / 2;

	let a = '' + n_s.split('').slice(0, limit),
		b = '' + n_s.split('').reverse().slice(0, limit);

	// console.log(n + ': ' + a + ' - ' + b + ' => ' + (a === b));
	return (a === b);
}

console.log(getLargestPalindrome(3));

// [123, 2345, 121, 2552].map((n) => {console.log(n + ' => ' + isPalindrome(n));});