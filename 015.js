'use strict';

function fac(n) {
    let val = 1,
        i = 2;

    while(i <= n) {
        val = val * i;
        i = i + 1;
    }

    return val;
}

function binomial(n, k) {
    return fac(n) / (fac(n - k) * fac(k));
}

function lattice(n) {
    return binomial(2 * n, n);
}

console.log(lattice((process.argv[2] || 0)));
// console.log(fac(process.argv[2] || 0));