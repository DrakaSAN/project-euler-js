'use strict';

//Taken from http://stackoverflow.com/a/5530230/2587646

let ones=['','one','two','three','four','five','six','seven','eight','nine'],
    tens=['','','twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety'],
    teens=['ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen'];

function convert_millions(num){
    if (num>=1000000){
        return convert_millions(Math.floor(num/1000000)) + 
            " million " + 
            ((num % 1000000 > 0)?'and ':'') +
            convert_thousands(num%1000000);
    }
    else {
        return convert_thousands(num);
    }
}

function convert_thousands(num){
    if (num>=1000){
        return convert_hundreds(Math.floor(num/1000)) +
            " thousand " +
            ((num % 1000 > 0)?'and ':'') +
            convert_hundreds(num%1000);
    }
    else{
        return convert_hundreds(num);
    }
}

function convert_hundreds(num){
    if (num>99){
        return ones[Math.floor(num/100)] + 
            " hundred " + 
            ((num % 100 > 0)?'and ':'') +
            convert_tens(num%100);
    }
    else{
        return convert_tens(num);
    }
}

function convert_tens(num){
    if (num<10) return ones[num];
    else if (num>=10 && num<20) return teens[num-10];
    else{
        return tens[Math.floor(num/10)]+" "+ones[num%10];
    }
}

function convert(num){
    if (num==0) return "zero";
    else return convert_millions(num);
}

function clean(w) {
    return w.split(' ').join('');
}

let i = 1,
    limit = process.argv[2] || 1,
    w = [];

while(i <= limit) {
    w.push(convert(i));
    i = i + 1;
}

w.map((w) => {console.log(w);});

console.log(w.map((w) => {return clean(w).length;}).reduce((acc, val) => {return acc + val;}, 0));

/*
let w = clean(convert(process.argv[2] || 1));

console.log(w + ' => ' + w.length);
*/